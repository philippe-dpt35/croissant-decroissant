Adaptation de l'application croissant-décroissant de la compilation d'applications clicmenu.

Ranger des nombres dans l'ordre croissant ou décroissant. Différents niveaux sont proposés.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/croissant-decroissant/index.html)
