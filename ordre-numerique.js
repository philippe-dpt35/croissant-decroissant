"use strict"

const paramListes = {
  nbItems: 8,
  niveau: -1,
  ordre: "croissant"
}

const paramPositionnement = {
  dernierSurvol: {},
  ctEspaces: 0,
  espaceWidth: "1.07%",
  coefEspaceWidth: 0.01,
  image: "url(img/attache3.png)"
}

var boutons = [];
var solution = [];

const receptacle = document.getElementById("receptacle");
const consigneClassement = document.getElementById("ordre");

function alea(min,max) {
  let nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function tireNombres() {
  let ct = 0;
  let nombre;
  let seuil = 100;
  let tableau = [];
  switch(paramListes.niveau) {
    case 0:
      seuil = 100;
      break;
    case 1:
      seuil = 1000;
      break;
    case 2:
      seuil = 10000;
      break;
    case 3:
      seuil = 1000000;
      break;
    case 4:
      seuil = 10000000000;
      break;
  }
  while (ct < paramListes.nbItems) {
    nombre = alea(0,seuil);
    if (tableau.indexOf(nombre) < 0) {
      tableau.push(nombre);
      ct +=1;
    }
  }
  return tableau;
}

function nouvelEspace(idItem) {
  const newEspace = document.createElement("div");
  newEspace.id = "espace-"+idItem;
  paramPositionnement.ctEspaces += 1;
  newEspace.className = "espace";
  newEspace.addEventListener("dragover", auSurvol);
  newEspace.addEventListener("dragleave", finSurvol);
  return newEspace;
}

function insereToit() {
  const toit = document.createElement("div");
  toit.id = "toit";
  receptacle.appendChild(toit);
}

function getPremiereDifference(listeReference,listeComparee) {
  let i = 0;
  let nbMots = 1;
  let difference = false;
  while ((i < listeReference.length) && (! difference)) {
    if (listeReference[i] === ",") {
      nbMots += 1;
    }
    if (listeReference[i] !== listeComparee[i]) {
      difference = true;
    }
    i += 1;
  }
  if (difference) {return nbMots}
  else {return 0}
}

// Pour savoir à quel élément est lié l'espace survolé
function getIdItemEspace(idObjet) {
  let idEspace = "";
  let tableau = idObjet.split("-");
  idEspace= tableau[1];
  return idEspace;
}

function nouvelleSerie() {
  let i;
  const reserve = document.getElementById("reserve");
  let listeItems = [];
  let tabOrdonne = [];
  reinitialise();
  listeItems = tireNombres();
  tabOrdonne = listeItems.slice();
  tabOrdonne.sort(function(a, b) {
    return a - b;
  });
  if (paramListes.ordre === "decroissant") {
    tabOrdonne.reverse();
  }
  for (i = 0; i < tabOrdonne.length; i++) {
    tabOrdonne[i] = new Intl.NumberFormat('fr-FR').format(tabOrdonne[i]);
    listeItems[i] = new Intl.NumberFormat('fr-FR').format(listeItems[i]);
  }
  solution = tabOrdonne.join();
  for (i = 0; i < listeItems.length; i++) {
    const newDiv = document.createElement("div");
    newDiv.id = "item" + i;
    newDiv.className = "draggable";
    newDiv.setAttribute('draggable', 'true');
    newDiv.addEventListener("dragstart", drag);
    const newDivTexte = document.createElement("div");
    newDivTexte.className = "div-texte";
    const newTexte = document.createTextNode(listeItems[i]);
    newDivTexte.appendChild(newTexte);
    newDiv.appendChild(newDivTexte);
    reserve.appendChild(newDiv);
  }
  document.getElementById("btitems").disabled = true;
  document.getElementById("btniveau").disabled = true;
  document.getElementById("btverifier").disabled = false;
}

function getIdObjet(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[0];
}

function getIdParent(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[1];
}

function  reinitialise() {
  let i;
  while (receptacle.firstChild) {
    receptacle.removeChild(receptacle.firstChild);
  }
  receptacle.innerHTML = "<div id='base'></div>";
}

function afficheConsigne() {
  if (paramListes.ordre === "decroissant") {
    consigneClassement.innerHTML = "grands.";
  }
  else {
    consigneClassement.innerHTML = "petits.";
  }
}

function choixNiveau() {
  document.getElementById("choix-niveau").className = "visible";
}

function lanceNiveau(ev) {
  let i;
  const listeNiveaux = document.getElementsByName('niveau');
  const ordreClassement = document.getElementsByName('ordre');
  for(i = 0; i < listeNiveaux.length; i++){
    if (listeNiveaux[i].checked) {
      paramListes.niveau = parseInt(listeNiveaux[i].value);
    }
  }
  for(i = 0; i < ordreClassement.length; i++){
    if (ordreClassement[i].checked) {
      paramListes.ordre = ordreClassement[i].value;
    }
  }
  afficheConsigne();
  document.getElementById("choix-niveau").className = "invisible";
  ev.preventDefault();
}

function verification() {
  let i;
  let proposition = "";
  let ordreErreur = 0;
  for (i = 0; i < receptacle.childNodes.length; i++) {
    if (i + 1 < receptacle.childNodes.length) {
      if (receptacle.childNodes[i].className === "draggable") {proposition = proposition + receptacle.childNodes[i].childNodes[0].innerHTML + ",";}
      }
      else {
        if (receptacle.childNodes[i].className === "draggable") {proposition = proposition + receptacle.childNodes[i].childNodes[0].innerHTML;}
      }
  }
  if (proposition !== solution) {
    ordreErreur = getPremiereDifference(solution,proposition);
    showDialog('<p>Malheureusement, il y a des erreurs à partir de l\'étage '+ ordreErreur + ' !</p><p>Tu peux redéplacer les étages vers le haut ou vers le bas pour corriger.</p>');
    return;
  }
  document.getElementById("btverifier").disabled = true;
  insereToit();
  document.getElementById("btitems").disabled = false;
  showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
  document.getElementById("btniveau").disabled = false;
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
	let infosObjet;
	infosObjet = ev.target.id + "-" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function auSurvol(ev) {
  ev.preventDefault();
  if (ev.target === paramPositionnement.dernierSurvol) {return;} // Evite de boucler l'opération tant que l'on reste sur la même zone
  ev.target.className = "espace-large";
  paramPositionnement.dernierSurvol = ev.target;
}

function finSurvol(ev) {
  ev.target.className = "espace";
  paramPositionnement.dernierSurvol = null;
}

function drop(ev) {
  ev.preventDefault();
  const idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  const idParent = getIdParent(ev.dataTransfer.getData('Text'));
  const Objet = document.getElementById(idObjet);
  if (ev.target.id === "receptacle") {  // L'élément est lâché sur une zone libre du réceptacle
      ev.target.appendChild(nouvelEspace(idObjet));
      if (idParent === "receptacle") {receptacle.removeChild(document.getElementById("espace-"+ Objet.id));}// On supprime l'espace lié à l'élément en cas de déplacement au sein de la zone réceptacle
      ev.target.appendChild(Objet);
    }
    else { // ou est lâché sur un espace entre éléments
      if ((ev.target.className !== "espace") && (ev.target.className !== "espace-large")) {return;}
      ev.target.className = "espace";
      if (getIdItemEspace(ev.target.id) === idObjet) {return;} // Si l'on survole l'espace lié à l'élément lui-même, on ne déplace pas
      if (idParent === "receptacle") {receptacle.removeChild(document.getElementById("espace-"+ Objet.id));} // On supprime l'espace lié à l'élément en cas de déplacement au sein de la zone réceptacle
      ev.target.parentNode.insertBefore(nouvelEspace(idObjet), ev.target);
      ev.target.parentNode.insertBefore(Objet, ev.target);
  }
  paramPositionnement.dernierSurvol = null;
}

choixNiveau();